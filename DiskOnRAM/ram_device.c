#include <linux/types.h>
#include <linux/vmalloc.h>
#include <linux/string.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <net/sock.h>
#include <linux/mutex.h>
#include "../kernel_socket/ksocket.h"
#include "misc.h"


#include "ram_device.h"
#include "partition.h"


#define RB_DEVICE_SIZE 1024 /* sectors */
/* So, total device size = 1024 * 512 bytes = 512 KiB */
#define READ_COMMAND 222
#define WRITE_COMMAND 333
#define READ_REQUEST_SIZE (2*sizeof(unsigned long) + sizeof(unsigned int))
#define WRITE_REQUEST_SIZE (2*sizeof(unsigned long) + sizeof(unsigned int))


/* Array where the disk stores its data */
//static u8 *dev_data;
static k_socket_t sockfd_cli;
//static struct mutex tx_lock;

int ramdevice_init(void) {
    struct sockaddr_in addr_srv;
    char buf[512], *tmp;
    int addr_len;
    //static k_socket_t sockfd_cli;
    //int len = 0;
    unsigned long command = 111;
    unsigned long response = -1;

#ifdef KSOCKET_ADDR_SAFE
    mm_segment_t old_fs;
    old_fs = get_fs();
    set_fs(KERNEL_DS);
#endif

    //sprintf(current->comm, "ksktcli");

    memset(&addr_srv, 0, sizeof (addr_srv));
    addr_srv.sin_family = AF_INET;
    addr_srv.sin_port = htons(remote_server_port);
    addr_srv.sin_addr.s_addr = k_inet_addr(remote_server_ip); // k_inet_addr("192.168.248.1"); //k_inet_addr("192.168.0.190"); //k_inet_addr("127.0.0.1");

    addr_len = sizeof (struct sockaddr_in);

    sockfd_cli = k_socket(AF_INET, SOCK_STREAM, 0);
    printk("sockfd_cli = 0x%p\n", sockfd_cli);

    if (sockfd_cli == NULL) {
        printk("socket failed\n");
        return -1;
    }
    if (k_connect(sockfd_cli, (struct sockaddr*) &addr_srv, addr_len) < 0) {
        printk("connect failed\n");
        return -1;
    }

    tmp = k_inet_ntoa(&addr_srv.sin_addr);
    printk("connected to : %s %d\n", tmp, ntohs(addr_srv.sin_port));
    kfree(tmp);


    memcpy(buf, &command, sizeof (unsigned long));


    //len = sprintf(buf, "%s", "Hello there .... server!\n");
    printk("sending command: <%lu> \n", command);
    if (k_send(sockfd_cli, buf, sizeof (unsigned long), 0) < 0)
        printk("Something happened while sending\n");

    printk("Done sending message\n");

    k_recv(sockfd_cli, buf, 1024, 0);
    memcpy(&response, buf, sizeof (unsigned long));

    printk("client got message : %lu\n", response);

    //spin_lock_init(&lock);
    //mutex_init(&tx_lock);


    //k_close(sockfd_cli);
#ifdef KSOCKET_ADDR_SAFE
    set_fs(old_fs);
#endif

    return response;
}

void ramdevice_cleanup(void) {
    //k_close(sockfd_cli);
}

void ramdevice_write(sector_t sector_off, u8 *buffer, unsigned int sectors) {
    unsigned long command_req, sector_off_req;
    unsigned int sector_count_req = 0;
    int total_size = 0;
    char * request_buffer = NULL;
    //int i = 0;
    //k_socket_t sockfd_cli;
    //struct sockaddr_in addr_srv;
    //int addr_len;

    command_req = WRITE_COMMAND;
    sector_off_req = sector_off;
    sector_count_req = sectors;

    /////////////////////////////////////////////////////////////////
    /*
    memset(&addr_srv, 0, sizeof (addr_srv));
    addr_srv.sin_family = AF_INET;
    addr_srv.sin_port = htons(remote_server_port);
    addr_srv.sin_addr.s_addr = k_inet_addr(remote_server_ip); // k_inet_addr("192.168.248.1"); //k_inet_addr("192.168.0.190"); //k_inet_addr("127.0.0.1");

    addr_len = sizeof (struct sockaddr_in);

    sockfd_cli = k_socket(AF_INET, SOCK_STREAM, 0);
    printk("sockfd_cli = 0x%p\n", sockfd_cli);

    if (sockfd_cli == NULL) {
        printk("socket failed\n");
        //return -1;
    }
    if (k_connect(sockfd_cli, (struct sockaddr*) &addr_srv, addr_len) < 0) {
        printk("connect failed\n");
        //return -1;
    }
    */
    /////////////////////////////////////////////////////////////////////

    total_size = (sectors * RB_SECTOR_SIZE) + sizeof (command_req) +
            sizeof (sector_off_req) + sizeof (sector_count_req);

    if (total_size)
        request_buffer = kmalloc(total_size, GFP_KERNEL);

    memcpy(request_buffer, &command_req, sizeof (command_req));
    memcpy(request_buffer + sizeof (command_req), &sector_off_req,
            sizeof (sector_off_req));
    memcpy(request_buffer + sizeof (command_req) + sizeof (sector_off_req),
            &sector_count_req, sizeof (sector_count_req));

    //mutex_lock(&tx_lock);
    memcpy(request_buffer + sizeof (command_req) + sizeof (sector_off_req) +
            sizeof (sector_count_req), buffer, sectors * RB_SECTOR_SIZE);

    printk("writing to sector <%lu> \n", sector_off);

    //spin_lock(&lock);
    
    /*
    if (sector_off == 0) {
        printk("------WRITE-------\n");
        for (i = 0; i < (sectors * RB_SECTOR_SIZE); i += sizeof (int)) {
            printk("%d", buffer[i]);
            if (i % 256 == 0)
                printk("\n");
        }
        printk("\n-------------\n");
    }*/
    if (k_send(sockfd_cli, request_buffer, total_size, 0) < 0)
        printk("Something happened while sending\n");

    //mutex_unlock(&tx_lock);
    //k_close(sockfd_cli);
    //spin_unlock(&lock);
}

void ramdevice_read(sector_t sector_off, u8 *buffer, unsigned int sectors) {


    unsigned long command_req, sector_off_req;
    unsigned int sector_count_req = 0;
    int total_size = 0;
    int n = 0;
    int read_so_far = 0;
    int bytes_expected = 0;
    char *request_buffer = buffer;
    //int i = 0;
    
    char command_request[READ_REQUEST_SIZE];
    
    //k_socket_t sockfd_cli;
    //struct sockaddr_in addr_srv;
    //int addr_len;

    command_req = READ_COMMAND;
    sector_off_req = sector_off;
    sector_count_req = sectors;

    /*
    if (sectors > 0) {
        request_buffer = kmalloc(sectors * RB_SECTOR_SIZE, GFP_KERNEL);
    } else {
        request_buffer = kmalloc(8 * RB_SECTOR_SIZE, GFP_KERNEL);
    }
     */

    /////////////////////////////////////////////////////////////////
    /*
    memset(&addr_srv, 0, sizeof (addr_srv));
    addr_srv.sin_family = AF_INET;
    addr_srv.sin_port = htons(remote_server_port);
    addr_srv.sin_addr.s_addr = k_inet_addr(remote_server_ip); // k_inet_addr("192.168.248.1"); //k_inet_addr("192.168.0.190"); //k_inet_addr("127.0.0.1");

    addr_len = sizeof (struct sockaddr_in);

    sockfd_cli = k_socket(AF_INET, SOCK_STREAM, 0);
    printk("sockfd_cli = 0x%p\n", sockfd_cli);

    if (sockfd_cli == NULL) {
        printk("socket failed\n");
        //return -1;
    }
    if (k_connect(sockfd_cli, (struct sockaddr*) &addr_srv, addr_len) < 0) {
        printk("connect failed\n");
        //return -1;
    }
    */
    /////////////////////////////////////////////////////////////////////

    memcpy(command_request, &command_req, sizeof (command_req));
    memcpy(command_request + sizeof (command_req), &sector_off_req,
            sizeof (sector_off_req));
    memcpy(command_request + sizeof (command_req) + sizeof (sector_off_req),
            &sector_count_req, sizeof (sector_count_req));

    printk("Reading from sector <%lu> \n", sector_off);

    total_size = (sectors * RB_SECTOR_SIZE) + sizeof (command_req) +
            sizeof (sector_off_req) + sizeof (sector_count_req);
    //spin_lock(&lock);
    //mutex_lock(&tx_lock);
    if (k_send(sockfd_cli, command_request, READ_REQUEST_SIZE, 0) < 0)
        printk("Something happened while sending\n");
    
    printk("Done sending the read command\n");

    bytes_expected = sectors * RB_SECTOR_SIZE;
    read_so_far = 0;
    while (read_so_far != bytes_expected) {
        n = k_recv(sockfd_cli, request_buffer + read_so_far, bytes_expected - read_so_far, 0);
        read_so_far += n;
    }
    
    printk("Done reading the response from the nswap\n");

    if (read_so_far != (sectors * RB_SECTOR_SIZE))
        printk("mismatch in size!!!!!!!!!! n=%d\n", n);

    //memcpy(buffer, request_buffer, bytes_expected);
    //k_close(sockfd_cli);
    //mutex_unlock(&tx_lock);
    //kfree(sockfd_cli);

    /*
    if (sector_off == 0) {
        printk("------READ-------\n");
        for (i = 0; i < (sectors * RB_SECTOR_SIZE); i += sizeof (int)) {
            printk("%d", buffer[i]);
            if (i % 256 == 0)
                printk("\n");
        }
        printk("\n-------------\n");
    }
     */
}
