/* 
 * tcpserver.c - A simple TCP echo server 
 * usage: tcpserver <port>
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "partition.h"

#define BUFSIZE 1024*8

#define SECTOR_SIZE 512
#define U_LONG_SIZE sizeof(unsigned long)
#define U_INT_SIZE sizeof(unsigned int)
#define MIN_WRITE_REQUEST_SIZE (U_LONG_SIZE+U_LONG_SIZE+U_INT_SIZE+SECTOR_SIZE) 
#define MIN_RW_REQUEST_SIZE (U_LONG_SIZE+U_LONG_SIZE+U_INT_SIZE) 
#define SIZE_COMMAND 111
#define READ_COMMAND 222
#define WRITE_COMMAND 333

static unsigned long disk_size = 0;


#define MAX_THREAD_COUNT 100

struct thread_pool_t {
    pthread_t tid[MAX_THREAD_COUNT];
    int thread_count;
};

struct thread_func_arg_t {
    int id;
    int childfd; /* child socket */
    struct sockaddr_in clientaddr; /* client addr */
    char *buf; /* message buffer */
    char header_buff[MIN_RW_REQUEST_SIZE];
    char *hostaddrp; /* dotted decimal host addr string */
    struct hostent *hostp; /* client host info */
};


static struct thread_pool_t th_pool;

int start_new_thread(struct thread_pool_t *pool, const pthread_attr_t *attr,
        void *(*start_routine) (void *), void *arg) {
    int retVal = 0;

    if (pool == NULL)
        return -1;

    if (pool->thread_count < MAX_THREAD_COUNT) {
        retVal = pthread_create(&(pool->tid[pool->thread_count]), attr, start_routine, arg);
        pool->thread_count = pool->thread_count + 1;
        return retVal;
    }

    return -2;
}

/*
 * error - wrapper for perror
 */
void error(char *msg) {
    perror(msg);
    exit(1);
}

void* client_thread(void *data) {

    struct thread_func_arg_t *client_data = (struct thread_func_arg_t*) data;
    int n = 0;

    if (client_data->childfd < 0)
        error("ERROR on accept");

    /* 
     * gethostbyaddr: determine who sent the message 
     */
    client_data->hostp = gethostbyaddr((const char *) &client_data->clientaddr.sin_addr.s_addr,
            sizeof (client_data->clientaddr.sin_addr.s_addr), AF_INET);
    if (client_data->hostp == NULL)
        error("ERROR on gethostbyaddr ---- ");
    client_data->hostaddrp = inet_ntoa(client_data->clientaddr.sin_addr);
    if (client_data->hostaddrp == NULL)
        error("ERROR on inet_ntoa\n");
    printf("server established connection with %s (%s)\n",
            client_data->hostp->h_name, client_data->hostaddrp);


    /* 
     * read: read input string from the client
     */
    while (1) {
        //bzero(client_data->buf, BUFSIZE);
        n = read(client_data->childfd, client_data->header_buff, U_LONG_SIZE);
        if (n < 0) {
            error("ERROR reading from socket");

        } else {
            n = handle_request(client_data);
            if (n > 0) {
                n = write(client_data->childfd, client_data->buf, n);
                if (n < 0)
                    error("ERROR writing to socket");
            }
        }
    }
    //close(childfd);

    return NULL;
}

int main(int argc, char **argv) {
    int parentfd; /* parent socket */
    int childfd; /* child socket */
    int portno; /* port to listen on */
    int clientlen; /* byte size of client's address */
    struct sockaddr_in serveraddr; /* server's addr */
    struct sockaddr_in clientaddr; /* client addr */
    int optval; /* flag value for setsockopt */
    int err = 0;


    disk_size = ramdevice_init();
    th_pool.thread_count = 0;

    /* 
     * check command line arguments 
     */
    if (argc != 2) {
        fprintf(stderr, "usage: %s <port>\n", argv[0]);
        exit(1);
    }
    portno = atoi(argv[1]);

    /* 
     * socket: create the parent socket 
     */
    parentfd = socket(AF_INET, SOCK_STREAM, 0);


    if (parentfd < 0)
        error("ERROR opening socket");

    /* setsockopt: Handy debugging trick that lets 
     * us rerun the server immediately after we kill it; 
     * otherwise we have to wait about 20 secs. 
     * Eliminates "ERROR on binding: Address already in use" error. 
     */
    optval = 1;
    setsockopt(parentfd, SOL_SOCKET, SO_REUSEADDR,
            (const void *) &optval, sizeof (int));

    /*
     * build the server's Internet address
     */
    bzero((char *) &serveraddr, sizeof (serveraddr));

    /* this is an Internet address */
    serveraddr.sin_family = AF_INET;

    /* let the system figure out our IP address */
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    /* this is the port we will listen on */
    serveraddr.sin_port = htons((unsigned short) portno);

    /* 
     * bind: associate the parent socket with a port 
     */
    if (bind(parentfd, (struct sockaddr *) &serveraddr,
            sizeof (serveraddr)) < 0)
        error("ERROR on binding");

    /* 
     * listen: make this socket ready to accept connection requests 
     */
    if (listen(parentfd, 5) < 0) /* allow 5 requests to queue up */
        error("ERROR on listen");

    /* 
     * main loop: wait for a connection request, echo input line, 
     * then close connection.
     */
    clientlen = sizeof (clientaddr);
    while (1) {

        /* 
         * accept: wait for a connection request 
         */
        childfd = accept(parentfd, (struct sockaddr *) &clientaddr, &clientlen);
        struct thread_func_arg_t *arg = (struct thread_func_arg_t *)
                malloc(sizeof (struct thread_func_arg_t));
        arg->id = 0;
        arg->clientaddr = clientaddr;
        arg->childfd = childfd;
        err = start_new_thread(&th_pool, NULL, &client_thread, (void *) arg);

    }
}

/* 
 * Requests format  
 * 1. size request  : [Command(UL)]
 * 2. write request : [Command(UL)][sector_offset(UL)][sector_count(UI)][Buffer(char*)]
 * 3. read request  : [Command(UL)][sector_offset(UL)][sector_count(UI)][Buffer(char*)]
 */
int handle_request(struct thread_func_arg_t *client_data) {

    unsigned long command = 0;
    unsigned long sector_off = 0;
    unsigned int sector_count = 0;
    int expected_data_size = 0;
    int read_so_far;
    int response_size = 0;
    int n = 0;
    //char * buf = client_data->buf;
    char * header = client_data->header_buff;
    int request_size = 0;


    memcpy(&command, header, U_LONG_SIZE);

    if (command != SIZE_COMMAND) {

        n = read(client_data->childfd, client_data->header_buff + U_LONG_SIZE,
                MIN_RW_REQUEST_SIZE - U_LONG_SIZE);

        request_size = n + U_LONG_SIZE;

        if (request_size >= MIN_RW_REQUEST_SIZE) { //Read req is the smaller of the two

            //buf = (char*) realloc(SECTOR_SIZE*sector_count);


            memcpy(&sector_off, header + U_LONG_SIZE, U_LONG_SIZE);
            memcpy(&sector_count, header + U_LONG_SIZE + U_LONG_SIZE, U_INT_SIZE);
            
            client_data->buf = (char*) malloc(sector_count * SECTOR_SIZE);

            if (command == READ_COMMAND) {   
                
                ramdevice_read(sector_off, client_data->buf, sector_count);
                printf("Read  -- %lu\n", sector_off);
                response_size = sector_count * SECTOR_SIZE;

            } else if (command == WRITE_COMMAND) {

                expected_data_size = SECTOR_SIZE*sector_count;
                read_so_far = 0;

                while (read_so_far < expected_data_size) {
                    n = read(client_data->childfd, client_data->buf + read_so_far
                            , expected_data_size - read_so_far);
                    read_so_far += n;                    
                }
                            
                ramdevice_write(sector_off, client_data->buf , sector_count);
                printf("Write  -- %lu\n", sector_off);
                response_size = 0;
            }
        }
    } else {

        if (client_data->buf)
            free(client_data->buf);

        client_data->buf = (char*) malloc(U_LONG_SIZE);

        //*buf = disk_size;

        memcpy(client_data->buf, &disk_size, U_LONG_SIZE);
        //printf("server received %lu and replied with : %lu\n", command, disk_size);
        response_size = U_LONG_SIZE;
    }

    fflush(stdout);
    return response_size;

}