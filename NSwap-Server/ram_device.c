//#include <linux/types.h>
//#include <linux/vmalloc.h>
//#include <linux/string.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "ram_device.h"
#include "partition.h"

#define RB_DEVICE_SIZE 1024 /* sectors */
/* So, total device size = 1024 * 512 bytes = 512 KiB */

/* Array where the disk stores its data */
static u8 *dev_data;

int ramdevice_init(void) {
    dev_data = calloc(RB_DEVICE_SIZE * RB_SECTOR_SIZE, sizeof (u8));
    if (dev_data == NULL)
        return -1;
    /* Setup its partition table */
    copy_mbr_n_br(dev_data);
    return RB_DEVICE_SIZE;
}

void ramdevice_cleanup(void) {
    free(dev_data);
}

void ramdevice_write(sector_t sector_off, u8 *buffer, unsigned int sectors) {

    int i = 0;

    memcpy(dev_data + sector_off * RB_SECTOR_SIZE, buffer,
            sectors * RB_SECTOR_SIZE);
    
    
    if (1){//(sector_off == 0) {
        printf("------WRITE(%lu)-------\n", sector_off);
        for (i = 0; i < (sectors * RB_SECTOR_SIZE); i += sizeof (int)) {
            printf("%d", buffer[i]);
            if (i % 256 == 0)
                printf("\n");
        }
        printf("-------------\n");
    }
     

}

void ramdevice_read(sector_t sector_off, u8 *buffer, unsigned int sectors) {
    int i = 0;

    memcpy(buffer, dev_data + sector_off * RB_SECTOR_SIZE,
            sectors * RB_SECTOR_SIZE);

    /*
    if (0) {//(sector_off == 0) {
        printf("------READ(%lu)-------\n", sector_off);
        for (i = 0; i < (sectors * RB_SECTOR_SIZE); i += sizeof (int)) {
            printf("%d", buffer[i]);
            if (i % 256 == 0)
                printf("\n");
        }
        printf("-------------\n");
    }
     */


}
