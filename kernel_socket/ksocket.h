/* 
 * File:   signals.h
 * Author: eababneh
 *
 * Created on December 26, 2012, 1:33 PM
 */
#ifndef _ksocket_h_
#define _ksocket_h_

struct socket;
struct sockaddr;
struct in_addr;
typedef struct socket * k_socket_t;

k_socket_t      k_socket(int domain, int type, int protocol);
int             k_shutdown(k_socket_t socket, int how);
int             k_close(k_socket_t socket);

int             k_bind(k_socket_t socket, struct sockaddr *address, int address_len);
int             k_listen(k_socket_t socket, int backlog);
int             k_connect(k_socket_t socket, struct sockaddr *address, int address_len);
k_socket_t      k_accept(k_socket_t socket, struct sockaddr *address, int *address_len);

ssize_t         k_recv(k_socket_t socket, void *buffer, size_t length, int flags);
ssize_t         k_send(k_socket_t socket, const void *buffer, size_t length, int flags);
ssize_t         k_recvfrom(k_socket_t socket, void * buffer, size_t length, int flags, struct sockaddr * address, int * address_len);
ssize_t         k_sendto(k_socket_t socket, void *message, size_t length, int flags, const struct sockaddr *dest_addr, int dest_len);

int             k_getsockname(k_socket_t socket, struct sockaddr *address, int *address_len);
int             k_getpeername(k_socket_t socket, struct sockaddr *address, int *address_len);
int             k_setsockopt(k_socket_t socket, int level, int optname, void *optval, int optlen);
int             k_getsockopt(k_socket_t socket, int level, int optname, void *optval, int *optlen);

unsigned int    k_inet_addr(char* ip);
char *          k_inet_ntoa(struct in_addr *in); 

#endif /* !_k_socket_h_ */
