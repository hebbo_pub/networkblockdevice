/* 
 * File:   signals.h
 * Author: eababneh
 *
 * Created on December 26, 2012, 1:33 PM
 */
#ifndef _SXG_DEBUG_H_
#define _SXG_DEBUG_H_

/* debug macro*/
#ifdef  _sxg_debug_
#	define sxg_debug(fmt, args...)	printk("ksocket : %s, %s, %d, "fmt, __FILE__, __FUNCTION__, __LINE__, ##args)
#else
#	define sxg_debug(fmt, args...)
#endif

#endif /* !_SXG_DEBUG_H_ */
